from tkpatreon.config import config
from tkpatreon.api import api

try:
    config.load_config()
except Exception as ex:
    print("An error occurred while loading app configuration: %s" % ex)
    exit(1)

api.init()
