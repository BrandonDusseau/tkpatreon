from tkpatreon.api.database import queries
from tkpatreon.api.validation import typechecks


def validateImagePOST(payload):
    if payload is None:
        return "Request payload was empty"

    required_keys = {'title'}
    key_diff = list(required_keys - payload.keys())
    if key_diff != []:
        return "The following required properties are missing: " + ",".join(key_diff)

    if 'patreon_date' in payload and not typechecks.validateTimestamp(payload['patreon_date']):
        return "The provided date format is invalid"

    if 'patreon_link' in payload and not typechecks.validateUrl(payload['patreon_link']):
        return "Patreon link is not a valid URL"

    if payload.get('category') is not None:
        if not typechecks.isInt(payload['category']):
            return "Category must be an integer"
        category = queries.getSingleCategoryById(payload['category'])
        if category is None:
            return "The selected category does not exist"

    if payload.get('pools') is not None and not isinstance(payload['pools'], list):
        return "Pools parameter must be a list of integers"

    if payload.get('pools') is not None and len(payload['pools']) != 0:
        for pool in payload['pools']:
            if not typechecks.isInt(pool):
                return "Pools parameter must be a list of integers"
        pools = queries.getPoolsById(payload['pools'])
        if (len(pools) != len(payload['pools'])):
            return "One or more selected pools does not exist."

    if payload.get('tags') is not None and not isinstance(payload['tags'], list):
        return "Tags parameter must be a list of strings"

    return None
