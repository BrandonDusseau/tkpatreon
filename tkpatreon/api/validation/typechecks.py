import re


def isInt(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


def validateTimestamp(timestamp):
    pattern = r"^\d{4}-?\d{2}-?\d{2}(T\d{2}:?\d{2}:?\d{2}(Z|[+-]\d{2}(:?\d{2})?))?$"
    return re.match(pattern, timestamp) is not None


def validateUrl(url):
    return re.match(r"^https?://([^.]+\.)*[^/.]+(/.*)?$", url) is not None
