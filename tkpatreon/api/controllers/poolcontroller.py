from flask import jsonify, make_response
from tkpatreon.api.database import queries
from tkpatreon.api.util import translator
from tkpatreon.api.database.database import db


def getById(id):
    result = queries.getSinglePoolById(id)
    db.session.commit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translatePoolToObject(result))


def getAll():
    result = queries.getAllPools()
    db.session.commit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translatePoolListToObject(result))
