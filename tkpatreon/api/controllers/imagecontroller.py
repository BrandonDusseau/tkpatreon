from flask import jsonify, make_response
from tkpatreon.api.database.mapping import Image, Tag
from tkpatreon.api.database import queries
from tkpatreon.api.util import translator, idutils, sani
from tkpatreon.api.validation import imagevalidator
from tkpatreon.config import config
from tkpatreon.api.database.database import db
import dateutil.parser


def getById(id):
    result = queries.getSingleImageById(id)
    db.session.commit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translateImageToObject(result))


def getBySlug(slug):
    result = queries.getSingleImageBySlug(slug)
    db.session.commit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translateImageToObject(result))


def getAll():
    result = queries.getAllImages()
    db.commmit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translateImageListToObject(result))


def getAllInPool(id):
    result = queries.getAllImagesInPool(id)
    db.session.commit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translateImageListToObject(result))


def getAllInCategory(id):
    result = queries.getAllImagesInCategory(id)
    db.session.commit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translateImageListToObject(result))


def getAllWithTag(tag):
    result = queries.getAllImagesWithTag(tag)
    db.commmit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translateImageListToObject(result))


def createImage(payload):
    validation = imagevalidator.validateImagePOST(payload)
    if validation is not None:
        return make_response(jsonify({"error": validation}), 400)

    draft = buildDraft(payload)
    # db.session.add(draft)
    # db.session.commit()

    return buildCreateImageResponse(draft)


def buildCreateImageResponse(image):
    upload_url = "{}/image/upload/{}".format(
                                             config.get("api.host"),
                                             image.draft_id)
    resp_obj = {
        "upload_url": upload_url,
        "upload_id": image.draft_id
    }

    response = make_response(jsonify(resp_obj), 202)
    response.headers['Location'] = upload_url

    return response


def buildDraft(payload):
    draft = Image()
    draft.draft_id = generateDraftId(payload)
    draft.slug = generateSlug(payload)

    # Add tags
    if payload.get('tags') is not None:
        new_tags = getNewTags(payload['tags'])
        for tag in new_tags:
            tag_obj = Tag(name=tag)
            draft.tags.append(tag_obj)

    # Add remainder of metadata
    draft.title = payload['title']
    draft.description = (sani.escapeHtml(payload['description'])
                         if payload.get('description')
                         else None)
    draft.patreon_link = (sani.sanitizeUrl(payload['patreon_link'])
                          if payload.get('patreon_link')
                          else None)
    draft.transcript = (sani.escapeHtml(payload['transcript'])
                        if payload.get('transcript')
                        else None)
    draft.patreon_date_posted = (dateutil.parser.parse(payload['patreon_date'])
                                 if payload.get('patreon_date')
                                 else None)
    # draft.pools = ",".join(payload['pools']) if 'pools' in payload else None
    draft.category_id = (int(payload['category'])
                         if payload.get('category')
                         else None)
    draft.uuid = str(idutils.create_uuid())

    return draft


def getNewTags(taglist):
    existing_tags = queries.getTagsByName(taglist)
    new_tags = ()
    for tag in existing_tags:
        if tag.name not in taglist:
            new_tags.append(tag.name)
    print("New tags: " + ", ".join(new_tags))
    return new_tags


def generateDraftId(payload):
    draft_ids = queries.getImageDraftIds()

    draft_id = ""
    while draft_id == "" or draft_id in draft_ids:
        draft_id = idutils.create_alpha_id()
    return draft_id


def generateSlug(payload):
    slugs = queries.getImageSlugs()

    original_slug = payload['title']
    slug = original_slug
    counter = 1
    while slug in slugs:
        slug = original_slug + "-" + str(counter)
        counter += 1
    return slug
