from flask import jsonify, make_response
from tkpatreon.api.database import queries
from tkpatreon.api.util import translator
from tkpatreon.api.database.database import db


def getById(id):
    result = queries.getSingleCategoryById(id)
    db.session.commit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translateCategoryToObject(result))


def getAll():
    result = queries.getAllCategories()
    db.session.commit()

    if result is None:
        return make_response(jsonify({"error": "Not found"}), 404)

    return jsonify(translator.translateCategoryListToObject(result))
