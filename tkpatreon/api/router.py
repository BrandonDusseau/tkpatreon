from flask import request
from tkpatreon.api.controllers import imagecontroller, categorycontroller, poolcontroller


def setup_routes(app):
    # Fetch and manipulate a single image
    @app.route('/image/<int:id>', methods=['GET'])
    def getImageById(id):
        return imagecontroller.getById(id)

    # Fetch an image by its slug
    @app.route('/image/slug/<string:slug>', methods=['GET'])
    def getImageBySlug(slug):
        return imagecontroller.getBySlug(slug)

    # Fetch a list of all images
    @app.route('/images', methods=['GET'])
    def getAllImages():
        return imagecontroller.getAll()

    # Fetch all images in a given pool
    @app.route('/images/pool/<int:id>', methods=['GET'])
    def getAllImagesInPool(id):
        return imagecontroller.getAllInPool(id)

    # Fetch all images with a given category
    @app.route('/images/category/<int:id>', methods=['GET'])
    def getAllImagesInCategory(id):
        return imagecontroller.getAllInCategory(id)

    # Fetch all images with a given tag
    @app.route('/images/tag/<string:name>', methods=['GET'])
    def getAllImagesWithTag(name):
        return imagecontroller.getAllWithTag(name)

    # Create an image
    @app.route('/image', methods=['POST'])
    def createImage():
        return imagecontroller.createImage(request.get_json())

    # Fetch and manipulate a single category
    @app.route('/category/<int:id>', methods=['GET'])
    def getCategoryById(id):
        return categorycontroller.getById(id)

    # Fetch a list of all categories
    @app.route('/categories', methods=['GET'])
    def getAllCategories():
        return categorycontroller.getAll()

    # # Create a category
    # @app.route('/category', methods=['POST'])

    # Fetch and manipulate a single pool
    @app.route('/pool/<int:id>', methods=['GET'])
    def getPoolById(id):
        return poolcontroller.getById(id)

    # Fetch a list of all image pools
    @app.route('/pools', methods=['GET'])
    def getAllPools():
        return poolcontroller.getAll()
    #
    # # Create a pool
    # @app.route('/pool', methods=['POST'])
