from tkpatreon.api.database.mapping import Image, Category, Pool, Tag
from tkpatreon.api.database.database import db


def getSingleImageById(id, include_drafts=False):
    query = Image.query.filter(Image.id == id)

    if (not include_drafts):
        query.filter(Image.draft_id is None)

    result = query.first()
    return result


def getSingleImageBySlug(slug):
    query = (Image.query
             .filter(Image.slug == slug)
             .filter(Image.draft_id is None))
    result = query.first()
    return result


def getAllImages():
    query = (Image.query
             .filter(Image.draft_id is None))
    result = query.all()
    return result


def getAllImagesInPool(pool_id):
    query = (Image.query
             .join(Image.pools)
             .filter(Pool.id == pool_id)
             .filter(Image.draft_id is None))
    result = query.all()
    return result


def getAllImagesInCategory(category_id):
    query = (Image.query
             .filter(Image.category_id == category_id)
             .filter(Image.draft_id is None))
    result = query.all()
    return result


def getAllImagesWithTag(tag):
    query = (Image.query
             .join(Image.tags)
             .filter(db.func.lower(Tag.name) == db.func.lower(tag))
             .filter(Image.draft_id is None))
    result = query.all()
    return result


def getSingleCategoryById(id):
    query = (Category.query
             .filter(Category.id == id))
    result = query.first()
    return result


def getAllCategories():
    query = Category.query
    result = query.all()
    return result


def getSinglePoolById(id):
    query = (Pool.query
             .filter(Pool.id == id))
    result = query.first()
    return result


def getPoolsById(id_list):
    query = (Pool.query
             .filter(Pool.id.in_(id_list)))
    result = query.all()
    return result


def getAllPools():
    query = Pool.query
    result = query.all()
    return result


def getImageDraftIds():
    query = (Image.draft_id.query
             .filter(Image.draft_id is not None))
    result = [r.draft_id for r in query.all()]
    return result


def getImageSlugs():
    query = (Image.slug.query
             .filter(Image.slug is not None))
    result = [r.slug for r in query.all()]
    return result


def getTagByName(tag):
    query = (Tag.query
             .filter(Tag.name == tag))
    result = query.first()
    return result


def getTagsByName(tags):
    query = (Tag.query
             .filter(Tag.name.in_(tags)))
    result = query.all()
    return result
