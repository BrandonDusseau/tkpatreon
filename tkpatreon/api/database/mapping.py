from tkpatreon.api.database.database import db

image_pool_assoc = db.Table('images_pools', db.metadata,
                            db.Column(
                             'images_id', db.Integer, db.ForeignKey('images.id'), primary_key=True
                             ),
                            db.Column(
                             'pools_id', db.Integer, db.ForeignKey('pools.id'), primary_key=True
                             ),
                            db.Column('order', db.Integer)
                            )

image_tag_assoc = db.Table('images_tags', db.metadata,
                           db.Column(
                            'images_id', db.Integer, db.ForeignKey('images.id'), primary_key=True
                            ),
                           db.Column('tags_id', db.Integer, db.ForeignKey('tags.id'), primary_key=True)
                           )


class Tag(db.Model):
    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    images = db.relationship(
        "Image",
        secondary=image_tag_assoc,
        back_populates="tags",
        passive_deletes=True)


class Image(db.Model):
    __tablename__ = 'images'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    description = db.Column(db.TEXT)
    patreon_link = db.Column(db.String)
    date_added = db.Column(db.TIMESTAMP)
    date_updated = db.Column(db.TIMESTAMP)
    transcript = db.Column(db.TEXT)
    uuid = db.Column(db.String)
    patreon_date_posted = db.Column(db.TIMESTAMP)
    slug = db.Column(db.String)
    draft_id = db.Column(db.String)

    pools = db.relationship(
        "Pool",
        secondary=image_pool_assoc,
        back_populates="images",
        passive_deletes=True,
        lazy="joined")

    tags = db.relationship(
        "Tag",
        secondary=image_tag_assoc,
        back_populates="images",
        passive_deletes=True,
        lazy="joined")

    category_id = db.Column('category', db.Integer, db.ForeignKey('categories.id'))
    category = db.relationship("Category")


class Category(db.Model):
    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)


class Pool(db.Model):
    __tablename__ = 'pools'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    images = db.relationship(
        "Image",
        secondary=image_pool_assoc,
        back_populates="pools",
        passive_deletes=True)
