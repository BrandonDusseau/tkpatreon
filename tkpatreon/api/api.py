from flask import Flask
from tkpatreon.api.database import database
from tkpatreon.api.database.exceptions import DatabaseException
from tkpatreon.config import config
from tkpatreon.api import router
from urllib.parse import quote_plus as urlquote


def init():
    db_username = config.get("database.username")
    db_password = config.get("database.password")
    db_hostname = config.get("database.hostname")
    db_db = config.get("database.db")

    if not all((db_username, db_password, db_hostname, db_db)):
        raise DatabaseException("Database configuration is incomplete")

    db_uri = "postgresql+psycopg2://%s:%s@%s/%s" % (db_username, urlquote(db_password), db_hostname, db_db)

    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
    database.db.init_app(app)
    router.setup_routes(app)

    app.run()
