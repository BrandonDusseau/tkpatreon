from slugify import slugify
import random
import string
import uuid


def create_uuid():
    return uuid.uuid4()


def create_slug(text):
    return slugify(text, word_boundary=True, max_length=50)


def create_alpha_id():
    return "".join([random.choice(string.ascii_letters) for i in range(7)])
