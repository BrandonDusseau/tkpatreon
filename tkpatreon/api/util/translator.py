def translateImageToObject(image):
    image_dict = {
        "id": image.id,
        "title": image.title,
        "description": image.description,
        "patreon_link": image.patreon_link,
        "category": translateCategoryToObject(image.category),
        "date_added": image.date_added,
        "date_updated": image.date_updated,
        "transcript": image.transcript,
        "uuid": image.uuid,
        "slug": image.slug,
        "patreon_date_posted": image.patreon_date_posted
    }

    pools = []
    for pool in image.pools:
        pools.append(translatePoolToObject(pool))
    image_dict['pools'] = pools

    tags = []
    for tag in image.tags:
        tags.append(translateTagToObject(tag))
    image_dict['tags'] = tags

    return image_dict


def translateImageListToObject(images):
    list = []
    for image in images:
        list.append(translateImageToObject(image))
    return list


def translatePoolToObject(pool):
    return {
        "id": pool.id,
        "name": pool.name
    }


def translatePoolListToObject(pools):
    list = []
    for pool in pools:
        list.append(translatePoolToObject(pool))
    return list


def translateTagToObject(tag):
    return {
        "id": tag.id,
        "name": tag.name
    }


def translateCategoryToObject(category):
    return {
        "id": category.id,
        "name": category.name
    }


def translateCategoryListToObject(categories):
    list = []
    for category in categories:
        list.append(translateCategoryToObject(category))
    return list
