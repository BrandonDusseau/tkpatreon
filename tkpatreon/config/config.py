from functools import reduce
import operator
import os
import yaml

config = {}
config_path = os.path.join(os.path.dirname(__file__), "..", "..", "config.yml")


def load_config():
    global config
    with open(config_path, "r") as ymlfile:
        config = yaml.load(ymlfile)


def get(key):
    keylist = key.split(".")
    try:
        return reduce(operator.getitem, keylist, config)
    except (KeyError, IndexError):
        return None


def set(key, value):
    keylist = key.split(".")
    get(keylist[:-1])[keylist[-1]]

    with open(config_path, "w") as yml_file:
        yaml.dump(config, yml_file)
